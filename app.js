(function () {
    var MyApp = angular.module('MyApp', ['ngRoute']);
	
	MyApp.config(['$routeProvider',
		function($routeProvider) {
			$routeProvider.
			  when('/list', {
				templateUrl: 'list.html'
			  }).
			  when('/person/:id', {
				templateUrl: 'person-detail.html'
			  }).
			  otherwise({
				redirectTo: '/list'
			  });	
		}
	]);
	
    MyApp.directive('customModal', function () {
        return {
            restrict: 'E',
            transclude: true,
            scope: {
                show: '=',
                okText: '=',
                cancelText: '=',
                headerText: '=',
                cancel: '&onCancel',
                submit: '&onSubmit'
            },
            replace: 'true',
            templateUrl: 'custom-modal.html'
        };
    });
	
	MyApp.directive('phoneInput', function () {
        return {
            restrict: 'E',
            transclude: true,
            scope: {
                form: '=',
                phoneModel: '=',
				inputName: '@',
				labelText: '@'
            },
            replace: 'true',
            templateUrl: 'phone-input.html'
        };
    });

    MyApp.directive('notification', ['$timeout', function ($timeout) {
        return {
            restrict: 'E',
            template:"<div class='alert alert-{{alertData.type}}' ng-show='alertData.show' role='alert'>" +
                        "{{alertData.message}}<span class='cross pull-right' ng-click='alertData.show = false;'>X</span>" +
                    "</div>",
            scope: {
                alertData:"="
            },
            replace:true,
            link: function(scope, elem, attrs) {
                scope.$watch("alertData.show", function (value) {
                    if (value) {
                        $timeout(function () {
                            scope.alertData.show = false;
                        }, 5000);
                    }
                });
            }
        };
    }]);

	MyApp.controller('MainController', ['$scope', 'personsService', function ($scope, personsService) {
        $scope.notification = {};

        $scope.showSuccess = function(message) {
            $scope.notification.message = message;
            $scope.notification.type = 'success';
            $scope.notification.show = true;
        };

        $scope.showError = function(message) {
            $scope.notification.message = message;
            $scope.notification.type = 'danger';
            $scope.notification.show = true;
        };
    }]);

    MyApp.controller('PersonListController', ['$scope', '$sce', 'personsService', function ($scope, $sce, personsService) {
        var BTNDEFAULTCLASS = 'btn-default';
        var BTNPRIMARYCLASS = 'btn-primary';
        var BTNDEFAULTTEXT = 'Show Lesson Content';
        var BTNPRIMARYTEXT = 'Hide Lesson Content';
        var TRIANGLEBOTTOMCLASS = 'glyphicon glyphicon-arrow-down';
        var TRIANGLETOPCLASS = 'glyphicon glyphicon-arrow-up';

        getPersons();

        function getPersons() {
            personsService.getAllPersons()
                .success(function (persons) {
                    $scope.persons = persons;
                })
                .error(function (error) {
                    $scope.status = 'Unable to load customer data: ' + error.message;
                });
        }

        $scope.showTable = false;
        $scope.btnShowTableClass = 'btn-default';
        $scope.buttonTxt = BTNDEFAULTTEXT;

        $scope.sortParameter = 'firstName';
        $scope.sortOrder = false;

        $scope.editModalShown = false;
        $scope.deleteModalShown = false;
        $scope.addModalShown = false;

        $scope.modalOkText = 'Submit';
        $scope.modalCancelText = 'Close Window';

        $scope.deleteModalOkText = 'Delete';
        $scope.deleteModalCancelText = 'Cancel';

        $scope.addModalOkText = 'Add';
        $scope.addModalCancelText = 'Cancel';

        $scope.phoneTypeVisible = 'phoneNumber';

        $scope.selectedPerson;
        $scope.lastViewedPerson;

        $scope.toggleTable = function () {
            if ($scope.btnShowTableClass == BTNDEFAULTCLASS) {
                $scope.btnShowTableClass = BTNPRIMARYCLASS;
                $scope.buttonTxt = BTNPRIMARYTEXT;
            } else {
                $scope.btnShowTableClass = BTNDEFAULTCLASS;
                $scope.buttonTxt = BTNDEFAULTTEXT;
            }

            $scope.showTable = !$scope.showTable;
        };

        $scope.sortBy = function (sortParameter) {
            if ($scope.sortParameter != sortParameter) {
                $scope.sortOrder = false;
            } else {
                $scope.sortOrder = !$scope.sortOrder;
            }

            $scope.sortParameter = sortParameter;
        };

        $scope.sortIconClass = function (sortParameter) {
            var sClass = $scope.sortParameter === sortParameter
                ? $scope.sortOrder === false
                    ? TRIANGLEBOTTOMCLASS
                    : TRIANGLETOPCLASS
                : '';
            return sClass;
        };

        // ADD PERSON
        $scope.addablePerson;

        $scope.toggleAddModal = function () {
            $scope.addablePerson = {};
            $scope.modalHeaderText = $sce.trustAsHtml('<i>Add Person</i>');

            $scope.addModalShown = !$scope.addModalShown;
        };

        $scope.hideAddModal = function () {
            $scope.addModalShown = false;
        };

        $scope.addPerson = function (person) {
            $scope.personAddForm.$setSubmitted();
            if ($scope.personAddForm.$valid) {
                personsService.addPerson(person)
                    .success(function () {
                        $scope.persons.push(person);
                        $scope.showSuccess('User was successfully added.');
                    })
                    .error(function (error) {
                        $scope.showError('Server error. Cannot add user.');
                    });

                $scope.addModalShown = false;
            }
        };

        // EDIT PERSON
        $scope.editablePerson;

        $scope.toggleEditModal = function (person) {
            $scope.editablePerson = angular.copy(person);
            $scope.modalHeaderText = $sce.trustAsHtml('<i>' + $scope.editablePerson.firstName + ' ' + $scope.editablePerson.lastName + '</i>');

            $scope.editModalShown = !$scope.editModalShown;
        };

        $scope.hideEditModal = function () {
            $scope.editModalShown = false;
            $scope.lastViewedPerson = null;
        };

        $scope.updatePerson = function (person) {
            $scope.personEditForm.$setSubmitted();
            if ($scope.personEditForm.$valid) {
                personsService.addPerson(person)
                    .success(function () {
                        var personToUpdate = $scope.persons.filter(function (p) { return p.id == person.id; } )[0];
                        var indexOfPersonToUpdate = $scope.persons.indexOf(personToUpdate);
                        $scope.persons[indexOfPersonToUpdate] = angular.copy(person);
                        $scope.showSuccess('User was successfully updated.');
                    })
                    .error(function (error) {
                        $scope.showError('Server error. Cannot update user.');
                    });

                $scope.editModalShown = false;
            }
        };

        // DELETE PERSON
        $scope.toggleDeleteModal = function (person) {
            $scope.selectedPerson = person;
            $scope.modalHeaderText = $sce.trustAsHtml('<i>Delete Person</i>');

            $scope.deleteModalShown = !$scope.deleteModalShown;
        };

        $scope.hideDeleteModal = function () {
            $scope.deleteModalShown = false;
        };

        $scope.deletePerson = function (person) {
            personsService.deletePerson(person.id)
                .success(function (persons) {
                    $scope.persons = $scope.persons.filter(function (p) { return p.id != person.id; } );
                    $scope.showSuccess('User was successfully deleted.');
                })
                .error(function (error) {
                    $scope.showError('Server error. Cannot delete user.');
                });

            $scope.deleteModalShown = false;
        };
	}]);
	
    MyApp.controller('PersonDetailsController', ['$scope', '$routeParams', '$location', 'personsService', function ($scope, $routeParams, $location, personsService) {
        $scope.editablePerson;
        $scope.originalPerson;

		personsService.getAllPersons()
            .success(function (persons) {
                $scope.persons = persons;
                $scope.originalPerson = $scope.persons.filter(function (p) { return p.id == $routeParams.id; } )[0];
                $scope.editablePerson = angular.copy($scope.originalPerson);
            });

		$scope.updatePerson = function (updatedUser) {
			var personToUpdate = $scope.persons.filter(function (p) { return p.id == $routeParams.id; } )[0];
			var indexOfPersonToUpdate = $scope.persons.indexOf(personToUpdate);
			$scope.persons[indexOfPersonToUpdate] = angular.copy(updatedUser);
			
			$location.path('#/list');
		};
		
		$scope.checkNotModified = function (user) {
			return angular.equals(user, $scope.originalPerson);
		};
    }]);

    MyApp.factory('personsService', ['$http', function ($http) {
        var url = 'http://localhost:3000/persons';
        var personsService = {};

        personsService.getAllPersons = function() {
            return $http.get(url);
        };

        personsService.getPersonById = function (id) {
            return $http.get(url + '/' + id);
        };

        personsService.addPerson = function (p) {
            return $http.post(url, p);
        };

        personsService.updatePerson = function (p) {
            return $http.put(url + '/' + p.ID, p)
        };

        personsService.deletePerson = function (id) {
            return $http.delete(url + '/' + id);
        };

        return personsService;
    }]);
})(); 